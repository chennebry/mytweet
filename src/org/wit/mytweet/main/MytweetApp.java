package org.wit.mytweet.main;

import static org.wit.android.helpers.LogHelpers.info;

import org.wit.mytweet.models.TweetList;
import org.wit.mytweet.models.TweetListSerializer;

import android.app.Application;

public class MytweetApp extends Application

{
  private static final String FILENAME = "tweetList.json";

  public TweetList tweetList;

  @Override
  public void onCreate()
  {
    super.onCreate();
    TweetListSerializer serializer = new TweetListSerializer(this, FILENAME);
    tweetList = new TweetList(serializer);

    info(this, "MyTweet app launched");
  }

}
