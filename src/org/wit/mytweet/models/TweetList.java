package org.wit.mytweet.models;

import static org.wit.android.helpers.LogHelpers.info;

import java.util.ArrayList;
import java.util.UUID;

import android.util.Log;

public class TweetList
{
  public ArrayList<Tweet> tweets;
  private TweetListSerializer serializer;

  public TweetList()
  {
    tweets = new ArrayList<Tweet>();
  }

  public TweetList(TweetListSerializer serializer)
  {
    this.serializer = serializer;
    try
    {
      tweets = serializer.loadTweets();
    }
    catch (Exception e)
    {
      info(this, "Error loading residences: " + e.getMessage());
      tweets = new ArrayList<Tweet>();
    }
  }

  public boolean saveTweets()
  {
    try
    {
      serializer.saveTweets(tweets);
      info(this, "Tweets saved to file");
      return true;
    }
    catch (Exception e)
    {
      info(this, "Error saving tweets: " + e.getMessage());
      return false;
    }
  }

  public void addTweet(Tweet tweet)
  {
    tweets.add(tweet);
  }

  public Tweet getTweet(UUID id)
  {
    Log.i(this.getClass().getSimpleName(), "UUID parameter id: " + id);

    for (Tweet tweet : tweets)
    {
      if (id.equals(tweet.id))
      {
        return tweet;
      }
    }
    info(this, "failed to find tweet. returning first element array to avoid crash");
    return null;
  }

  public void clear()
  {
    tweets.clear();
  }

  public void deleteTweet(Tweet c)
  {
    tweets.remove(c);
  }
}
