package org.wit.mytweet.models;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;
import org.wit.mytweet.R;

import android.content.Context;

public class Tweet
{
  public UUID id;

  public String tweetText;
  public Date tweetDate;
  public String recipient;

  private static final String JSON_ID = "id";
  private static final String JSON_TWEETTEXT = "tweetText";
  private static final String JSON_TWEETDATE = "tweetDate";
  private static final String JSON_RECIPIENT = "recipient";

  public Tweet(String tweetText, Date tweetDate)
  {
    id = UUID.randomUUID();
    this.tweetText = tweetText;
    this.tweetDate = new Date();
    recipient = "No recipient selected yet";
  }

  public Tweet(JSONObject json) throws JSONException
  {
    id = UUID.fromString(json.getString(JSON_ID));
    tweetText = json.getString(JSON_TWEETTEXT);
    tweetDate = new Date(json.getLong(JSON_TWEETDATE));
    recipient = json.getString(JSON_RECIPIENT);
  }

  public JSONObject toJSON() throws JSONException
  {
    JSONObject json = new JSONObject();
    json.put(JSON_ID, id.toString());
    json.put(JSON_TWEETTEXT, tweetText);
    json.put(JSON_TWEETDATE, tweetDate.getTime());
    json.put(JSON_RECIPIENT, recipient);
    return json;
  }

  public String getDateString()
  {
    return "Sent: " + DateFormat.getDateTimeInstance().format(tweetDate);
  }

  public String getTweetReport(Context context)
  {
    String dateFormat = "EEE, MMM dd";
    String dateString = android.text.format.DateFormat.format(dateFormat, tweetDate).toString();
    String tweetRecipient = recipient;
    if (recipient == null)
    {
      tweetRecipient = context.getString(R.string.noTweetRecipient);
    }
    else
    {
      tweetRecipient = context.getString(R.string.currentTweetRecipient, recipient);
    }
    String report = "Tweet: " + tweetText + " Date: " + dateString + "To: " + tweetRecipient;
    return report;
  }

}
