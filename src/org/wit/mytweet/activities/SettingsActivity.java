package org.wit.mytweet.activities;

import org.wit.mytweet.R;
import android.app.Activity;
import android.os.Bundle;


public class SettingsActivity extends Activity
{

  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    SettingsFragment fragment = new SettingsFragment();
    getFragmentManager().beginTransaction().add(android.R.id.content, fragment, fragment.getClass().getSimpleName())
        .commit();
  }
}
