package org.wit.mytweet.activities;

import java.util.Date;
import java.util.UUID;

import org.wit.android.helpers.ContactHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.main.MytweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import static org.wit.android.helpers.IntentHelper.navigateUp;
import static org.wit.android.helpers.IntentHelper.sendEmail;

public class MytweetFragment extends Fragment implements OnClickListener, TextWatcher
{
  private TextView tweetDate;
  private TextView character_no;
  private Date date;
  private Button tweetButton;
  private EditText tweetText;
  private MytweetApp app;
  private TweetList tweetList;
  private Tweet tweet;
  private Button selectContactButton;
  private Button emailTweetButton;
  private static final int REQUEST_CONTACT = 1;
  public static final String EXTRA_TWEET_ID = "mytweet.TWEET_ID";

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    app = (MytweetApp) getActivity().getApplication();
    tweetList = app.tweetList;
    UUID id = (UUID) getArguments().getSerializable(EXTRA_TWEET_ID);
    tweet = tweetList.getTweet(id);
  }

  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
    View v = inflater.inflate(R.layout.fragment_mytweet, parent, false);

    getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
    addListeners(v);
    updateControls(tweet);

    return v;
  }

  public void addListeners(View v)
  {
    tweetDate = (TextView) v.findViewById(R.id.tweetDate);
    tweetButton = (Button) v.findViewById(R.id.tweetButton);
    character_no = (TextView) v.findViewById(R.id.character_no);
    tweetText = (EditText) v.findViewById(R.id.tweetText);
    emailTweetButton = (Button) v.findViewById(R.id.emailTweetButton);
    selectContactButton = (Button) v.findViewById(R.id.selectContactButton);

    tweetButton.setOnClickListener(this);
    tweetText.addTextChangedListener(this);
    emailTweetButton.setOnClickListener(this);
    selectContactButton.setOnClickListener(this);
  }

  public void updateControls(Tweet tweet)
  {
    tweetText.setText(tweet.tweetText);
    tweetDate.setText(tweet.getDateString());
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case android.R.id.home:
      navigateUp(getActivity());
      return true;
    default:
      return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onClick(View v)
  {
    switch (v.getId())
    {
    case R.id.tweetButton:
      tweetList.addTweet(new Tweet(tweetText.getText().toString(), date));
      Toast toast = Toast.makeText(getActivity(), "Message Sent", Toast.LENGTH_SHORT);
      toast.show();
      break;
    case R.id.emailTweetButton:
      sendEmail(getActivity(), selectContactButton.getText().toString(), getString(R.string.emailTweetSubject),
          tweetText.getText().toString());
      break;
    case R.id.selectContactButton:
      Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
      startActivityForResult(i, REQUEST_CONTACT);
      if (tweet.recipient != null)
      {
        selectContactButton.setText("To: " + tweet.recipient);
      }
      break;
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    if (resultCode != Activity.RESULT_OK)
    {
      return;
    }
    else
      if (requestCode == REQUEST_CONTACT)
      {
        String name = ContactHelper.getContact(getActivity(), data);
        tweet.recipient = name;
        selectContactButton.setText(name);
      }
  }

  @Override
  public void afterTextChanged(Editable tweetText)
  {
  }

  @Override
  public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3)
  {
  }

  @Override
  public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3)
  {
    character_no.setText(String.valueOf(140 - tweetText.length()));

    if (tweetText != null && tweetText.length() >= 140)
    {
      Toast toast = Toast.makeText(getActivity(), "Character Limit Exceeded", Toast.LENGTH_SHORT);
      toast.show();
    }
  }

  public void onPause()
  {
    super.onPause();
    if (tweetText.length() == 0)
    {
      tweetList.deleteTweet(tweet);
    }
    tweetList.saveTweets();
  }
}
