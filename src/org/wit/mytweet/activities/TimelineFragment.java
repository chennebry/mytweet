package org.wit.mytweet.activities;

import static org.wit.android.helpers.IntentHelper.startActivityWithData;

import java.util.ArrayList;

import org.wit.mytweet.R;
import org.wit.mytweet.main.MytweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class TimelineFragment extends ListFragment implements OnItemClickListener, MultiChoiceModeListener
{
  private ListView listView;;
  private TweetList tweetList;
  private MytweetApp app;
  private TimelineAdapter adapter;
  private ArrayList<Tweet> tweets;

  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    getActivity().setTitle(R.string.app_name);

    app = (MytweetApp) getActivity().getApplication();

    tweetList = app.tweetList;
    tweets = tweetList.tweets;
    adapter = new TimelineAdapter(getActivity(), tweets);
    setListAdapter(adapter);
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
  {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.timeline, menu);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
    View v = super.onCreateView(inflater, parent, savedInstanceState);
    listView = (ListView) v.findViewById(android.R.id.list);
    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    listView.setMultiChoiceModeListener(this);
    return v;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case R.id.menu_mytweet:
      Tweet tweet = new Tweet("", null);
      tweetList.addTweet(tweet);
      Intent i = new Intent(getActivity(), TweetPagerActivity.class);
      i.putExtra(MytweetFragment.EXTRA_TWEET_ID, tweet.id);
      startActivityForResult(i, 0);
      return true;

    case R.id.settings:
      startActivity(new Intent(getActivity(), SettingsActivity.class));
      return true;
    case R.id.clear:
      adapter.clear();
      return true;
    default:
      return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onResume()
  {
    super.onResume();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    ((TimelineAdapter) getListAdapter()).notifyDataSetChanged();
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id)
  {
    Tweet tweet = adapter.getItem(position);
    startActivityWithData(getActivity(), MytweetFragment.class, "TWEET_ID", tweet.id);

  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id)
  {
    Tweet tweet = ((TimelineAdapter) getListAdapter()).getItem(position);
    Intent i = new Intent(getActivity(), TweetPagerActivity.class);
    i.putExtra(MytweetFragment.EXTRA_TWEET_ID, tweet.id);
    startActivityForResult(i, 0);
  }

  private void deleteTweet(ActionMode mode)
  {
    for (int i = adapter.getCount() - 1; i >= 0; i--)
    {
      if (listView.isItemChecked(i))
      {
        tweetList.deleteTweet(adapter.getItem(i));
      }
    }
    mode.finish();
    adapter.notifyDataSetChanged();
  }

  @Override
  public boolean onActionItemClicked(ActionMode mode, MenuItem item)
  {
    switch (item.getItemId())
    {
    case R.id.menuDeleteTweet:
      deleteTweet(mode);
      return true;
    default:
      return false;
    }
  }

  @Override
  public boolean onCreateActionMode(ActionMode mode, Menu menu)
  {
    MenuInflater inflater = mode.getMenuInflater();
    inflater.inflate(R.menu.timeline_context, menu);
    return true;
  }

  @Override
  public void onDestroyActionMode(ActionMode arg0)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean onPrepareActionMode(ActionMode arg0, Menu arg1)
  {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void onItemCheckedStateChanged(ActionMode arg0, int arg1, long arg2, boolean arg3)
  {
    // TODO Auto-generated method stub

  }
}

class TimelineAdapter extends ArrayAdapter<Tweet>
{
  private Context context;

  public TimelineAdapter(Context context, ArrayList<Tweet> tweets)
  {
    super(context, R.layout.row_tweet, tweets);
    this.context = context;
  }

  @SuppressLint("InflateParams")
  @Override
  public View getView(int position, View convertView, ViewGroup parent)
  {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if (convertView == null)
    {
      convertView = inflater.inflate(R.layout.row_tweet, null);
    }

    Tweet tweet = getItem(position);

    TextView tweetText = (TextView) convertView.findViewById(R.id.row_tweetText);
    tweetText.setText(tweet.tweetText);

    TextView tweetDate = (TextView) convertView.findViewById(R.id.row_tweetDate);
    tweetDate.setText(tweet.getDateString());

    return convertView;
  }
}
